package com.notes.concessionaria_spdm

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.AlignmentLine
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusOrder
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.toSize
import com.notes.concessionaria_spdm.data.VehiclesSingleton
import com.notes.concessionaria_spdm.model.Vehicle
import com.notes.concessionaria_spdm.model.VehicleType
import com.notes.concessionaria_spdm.ui.theme.Concessionaria_SPDMTheme



class MainActivity : ComponentActivity() {

    @OptIn(ExperimentalMaterialApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Concessionaria_SPDMTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Column(
                        modifier = Modifier.fillMaxWidth()
                    ){
                        Input(LocalContext.current)
                        LazyListObject()
                    }
                }
            }
        }
    }
}


@Composable
fun Input(context: Context) {
    var model: String by remember {mutableStateOf("")}
    var type: String by remember {mutableStateOf("")}
    var price: String by remember {mutableStateOf("")}

    Card(modifier = Modifier
        .fillMaxWidth()
        .padding(5.dp),
        elevation = 10.dp
    ) {
        Column(
            Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(3.dp, alignment = Alignment.Top),
            horizontalAlignment = Alignment.CenterHorizontally
        ){
            model = TextInput()
            type = DropDownMenu()
            price = CustomTextField()
            Submit(context, model, type, price)
        }
    }
}

@Composable
fun TextInput() : String{
    var value: String by remember { mutableStateOf("") }

    OutlinedTextField(
        value = value,
        onValueChange = { it ->
            value = it
        },
        label = { Text(text="Model") },
        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp, 20.dp, 20.dp, 0.dp),
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Next,
            keyboardType = KeyboardType.Text
        ),
        visualTransformation = VisualTransformation.None
    )
    return value
}


@Composable
fun DropDownMenu(): String{
    var expanded by remember { mutableStateOf(false)}
    val list = listOf("HATCH", "TRUCK", "MOTORBIKE", "SEDAN", "PICKUP_TRUCK", "VAN", "SUV")
    var selectedItem by remember { mutableStateOf("") }
    var textFiledSize by remember { mutableStateOf(Size.Zero) }

    val icon = if(expanded){
        Icons.Filled.KeyboardArrowUp
    }else{
        Icons.Filled.KeyboardArrowDown
    }

    Column(
        modifier = Modifier.padding(20.dp, 0.dp, 20.dp, 0.dp)
    ){
        OutlinedTextField(
            value = selectedItem,
            onValueChange = { selectedItem = it},
            modifier = Modifier
                .fillMaxWidth()
                .onGloballyPositioned { coordinates ->
                    textFiledSize = coordinates.size.toSize()
                },
            label = { Text(text = "Choose a type...")},
            singleLine = true,
            readOnly = true,
            visualTransformation = VisualTransformation.None,
                    trailingIcon = {
                Icon(icon, "", Modifier.clickable { expanded = !expanded })
            }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false},
            modifier = Modifier.width(with(LocalDensity.current){textFiledSize.width.toDp()})
        ){
            list.forEach{ label ->
                DropdownMenuItem(onClick = {
                    selectedItem = label
                    expanded = false
                }){
                    Text(text = label)
                }
            }
        }
    }
    return selectedItem
}


@Composable
fun CustomTextField(): String{
    var text by remember{ mutableStateOf("") }

    OutlinedTextField(
        value = text,
        onValueChange = { it ->
            text = if (it.startsWith("0")){
                ""
            }else{
                it
            }
        },
        label = { Text(text="Price") },
        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp, 0.dp, 20.dp, 0.dp),
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.NumberPassword
        ),
        visualTransformation = CurrencyAmountInputVisualTransformation(),
    )
    return text
}


@Composable
fun Submit(context: Context, model: String, type: String, price: String){
    Button(
        onClick = {
            if(model.isNotEmpty() && type.isNotEmpty() && price.isNotEmpty()) {
                VehiclesSingleton.add(Vehicle(model, price.toFloat(), VehicleType.valueOf(type)))
                Toast.makeText(context, "Veículo adicionado.", Toast.LENGTH_SHORT).show()
            }else
                Toast.makeText(context, "Preencha todos os campos.", Toast.LENGTH_SHORT).show()
        },
        modifier = Modifier.padding(10.dp),
        enabled = true,
        border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
        shape = MaterialTheme.shapes.medium
    ) {
        Text(text = "Submit", color = Color.White)
    }
}

@ExperimentalMaterialApi
@Composable
fun LazyListObject(){
    val vehicles = VehiclesSingleton.getAll()

    LazyColumn(
        modifier = Modifier.padding(1.dp),
    ){
        itemsIndexed(vehicles){ index, item ->
            ListItem(index = index)
        }
    }
}


//@Preview(showBackground = true)
//@Composable
//fun DefaultPreview() {
//    Concessionaria_SPDMTheme {
//        LazyListObject()
//    }
//}