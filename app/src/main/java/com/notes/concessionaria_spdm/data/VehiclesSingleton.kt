package com.notes.concessionaria_spdm.data

import androidx.compose.runtime.*
import com.notes.concessionaria_spdm.model.Vehicle

object VehiclesSingleton {
    private val vehicles = mutableStateListOf<Vehicle>()
    private val elements : List<Vehicle> = vehicles

    fun add(v: Vehicle){
        this.vehicles.add(0, v)
    }

    fun getAll(): List<Vehicle> {
        return this.elements
    }

    fun sold(i: Int){
        this.vehicles[i].sold = !this.vehicles[i].sold
    }

    fun getItem(ind: Int): Vehicle{
        return this.vehicles[ind]
    }

}