package com.notes.concessionaria_spdm

import android.graphics.fonts.FontStyle
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.notes.concessionaria_spdm.data.VehiclesSingleton
import com.notes.concessionaria_spdm.model.Vehicle
import com.notes.concessionaria_spdm.model.VehicleType
import com.notes.concessionaria_spdm.ui.theme.Concessionaria_SPDMTheme

@OptIn(ExperimentalFoundationApi::class)
@ExperimentalMaterialApi
@Composable
fun ListItem(index : Int){
    var expanded by remember{ mutableStateOf(false)}
    val rotationState = if(expanded) 180f else 0f
    val vehicles = VehiclesSingleton.getAll()
    var sold by remember{ mutableStateOf(VehiclesSingleton.getItem(index).sold) }


    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .animateContentSize(
                animationSpec = tween(
                    durationMillis = 300,
                    easing = LinearOutSlowInEasing
                )
            )
            .combinedClickable (
                onLongClick = {
                    VehiclesSingleton.sold(index)
                    sold = !sold },
                onClick = {expanded = !expanded}
            ),
        border = BorderStroke(1.dp, Color.LightGray),
        elevation = 2.dp,
        shape = RoundedCornerShape(5.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ){
            Row(
                verticalAlignment = Alignment.CenterVertically,
            ){
                Text(
                    modifier = Modifier.weight(6f),
                    text = vehicles[index].model,
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1,
                    style =
                        if(sold)
                            TextStyle(textDecoration = TextDecoration.LineThrough)
                        else
                            TextStyle(textDecoration = TextDecoration.None)
                )
                if(sold) {
                    Text(
                        text = "Sold"
                    )
                }
                IconButton(
                    modifier = Modifier
                        .alpha(ContentAlpha.medium)
                        .weight(1f)
                        .rotate(rotationState),

                    onClick = {
                        expanded = !expanded
                    }
                ){
                    Icon(
                        imageVector = Icons.Default.ArrowDropDown,
                        contentDescription = "DropDown Arrow"
                    )
                }
            }
            if(expanded){
                Text(
                    text = "Type: " + vehicles[index].type.toString(),
                    fontSize = MaterialTheme.typography.subtitle1.fontSize,
                )
                Text(
                    text = "Price: " + vehicles[index].price.toString(),
                    fontSize = MaterialTheme.typography.subtitle1.fontSize,
                )
                Text(
                    text = "Status: " + if(sold) "Sold" else "Available",
                    fontSize = MaterialTheme.typography.subtitle1.fontSize,
                )
            }
        }
    }
}

