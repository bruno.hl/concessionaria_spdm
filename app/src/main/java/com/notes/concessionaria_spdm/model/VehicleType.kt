package com.notes.concessionaria_spdm.model

enum class VehicleType {
    HATCH, TRUCK, MOTORBIKE, SEDAN, PICKUP_TRUCK, VAN, SUV
}