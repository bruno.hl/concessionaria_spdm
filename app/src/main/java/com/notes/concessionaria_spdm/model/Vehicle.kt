package com.notes.concessionaria_spdm.model

data class Vehicle(
    val model: String,
    val price: Float,
    val type: VehicleType,
    var sold: Boolean = false
)
